import pandas as pd
#import uvicorn
import mlflow
import os
from dotenv import load_dotenv
from fastapi import FastAPI, File, UploadFile, HTTPException

load_dotenv()
app = FastAPI()

class Model:
    def __init__(self, model_name, model_stage):
        self.model = mlflow.pyfunc.load_model(f"models:/{model_name}/{model_stage}")

    def predict(self, data):
        predictions = self.model.predict(data)
        return predictions


model = Model("catboost", "latest")

@app.post("/invocation")
async def create_upload_file(file: UploadFile = File(...)):
    if file.filename.endswith(".csv"):
        with open(file.filename, "wb") as f:
            f.write(file.file.read())
        data = pd.read_csv(file.filename)
        os.remove(file.filename)
        return list(model.predict(data))
    else:
        raise HTTPException(
            status_code=400, detail="invalid file format, only CSV files accepted"
        )

if os.getenv("AWS_ACCESS_KEY_ID") is None or os.getenv("AWS_SECRET_ACCESS_KEY") is None:
    print('secret fail')
    exit(1)
